#include <Arduino.h>
#ifdef ARDUINO_AVR_UNO
#include <MIDI.h>
#else
#include <USB-MIDI.h>
#endif
#include "Instruments.h"

#ifdef ARDUINO_AVR_UNO
struct MySettings : public midi::DefaultSettings
{
  static const long BaudRate = 115200;
};
MIDI_CREATE_CUSTOM_INSTANCE(HardwareSerial, Serial, MIDI, MySettings);
#else
USBMIDI_CREATE_DEFAULT_INSTANCE();
#endif

nm::Oscillator buzzer(9, 4);
nm::CheapStepperOscillator stepper(1, 8, 9, 10, 11);
nm::ServoPulse servo(10, 5);
nm::OnToggle relay(11, 7);


// -----------------------------------------------------------------------------

// This function will be automatically called when a NoteOn is received.
// It must be a void-returning function with the correct parameters,
// see documentation here:
// https://github.com/FortySevenEffects/arduino_midi_library/wiki/Using-Callbacks

void handleNoteOn(byte channel, byte note, byte velocity)
{
    // Do whatever you want when a note is pressed.
    // Try to keep your callbacks short (no delays ect)
    // otherwise it would slow down the loop() and have a bad impact
    // on real-time performance.
    buzzer.noteOn(channel, note, velocity);
    stepper.noteOn(channel, note, velocity);
    servo.noteOn(channel, note, velocity);
    relay.noteOn(channel, note, velocity);
}

void handleNoteOff(byte channel, byte note, byte velocity)
{
    // Do something when the note is released.
    // Note that NoteOn messages with 0 velocity are interpreted as NoteOffs.
    buzzer.noteOff(channel, note);
    stepper.noteOff(channel, note);
    servo.noteOff(channel, note);
    relay.noteOff(channel, note);
}

// -----------------------------------------------------------------------------

void setup()
{
    // Connect the handleNoteOn function to the library,
    // so it is called upon reception of a NoteOn.
    MIDI.setHandleNoteOn(handleNoteOn);  // Put only the name of the function

    // Do the same for NoteOffs
    MIDI.setHandleNoteOff(handleNoteOff);

    // Initiate MIDI communications, listen to all channels
    MIDI.begin(MIDI_CHANNEL_OMNI);
}

void loop()
{
    // Call MIDI.read the fastest you can for real-time performance.
    MIDI.read();

    buzzer.update();
    stepper.update();
    servo.update();
    relay.update();
    // There is no need to check if there are messages incoming
    // if they are bound to a Callback function.
    // The attached method will be called automatically
    // when the corresponding message has been received.
}