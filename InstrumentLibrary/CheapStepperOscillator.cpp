/*
 *  StepperOscillator.cpp
 *
 *  Copyright (c) 2016, Neil Mendoza, http://www.neilmendoza.com
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Neil Mendoza nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "CheapStepperOscillator.h"

namespace nm
{
  const bool CheapStepperOscillator::PIN_STATES[4][4] = {{1,0,1, 0}, {0,1,1,0}, {0,1,0,1}, {1,0,0,1}};

  CheapStepperOscillator::CheapStepperOscillator(const unsigned channel, const unsigned pin0, const unsigned pin1, const unsigned pin2, const unsigned pin3) :
    Instrument(channel),
    pins{pin0, pin1, pin2, pin3}
  {
    for (auto& p : pins)
    {
      pinMode(p, OUTPUT);
      digitalWrite(p, LOW);
    }
  }

  void CheapStepperOscillator::noteOn(uint8_t note, uint8_t velocity)
  {
    float frequency = Instrument::midiToFrequency(note);
    periodMicros = frequencyDivisor * 1000000.f / frequency;
    onPeriodMicros = 0.5 * periodMicros;
  }

  void CheapStepperOscillator::noteOff(uint8_t note)
  {
    periodMicros = 0;
    onPeriodMicros = 0;
    state = LOW;    
    for (auto& p : pins) digitalWrite(p, LOW);
  }

  void CheapStepperOscillator::update()
  {
    if(periodMicros > 0)
    {
      unsigned long elapsedPeriodMicros = micros() % periodMicros;

      if((elapsedPeriodMicros > onPeriodMicros) == state)
      {
        // wave form has flipped
        state = !state;
        if (state) step();
      }
    }
  }

  void CheapStepperOscillator::step()
  {
    uint8_t currentStep = stepCounter % 4;
    for (unsigned i = 0; i < 4; ++i)
    {
      digitalWrite(pins[i], PIN_STATES[currentStep][i]);
    }
    stepCounter++;
  }
}
