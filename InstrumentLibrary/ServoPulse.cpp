/*
 *  OnPulse.cpp
 *
 *  Copyright (c) 2018, Neil Mendoza, http://www.neilmendoza.com
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Neil Mendoza nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "ServoPulse.h"
#include <Arduino.h>

namespace nm
{
  ServoPulse::ServoPulse(unsigned channel, unsigned servoPin, unsigned long holdTime = 120, int offAngle = 45, int onAngle = 135) :
    Instrument(channel),
    triggerTime(0),
    holdTime(holdTime),
    servoPin(servoPin),
    offAngle(offAngle),
    onAngle(onAngle),
    angle(offAngle)
  {
  }

  void ServoPulse::noteOn(uint8_t note, uint8_t velocity)
  {
    angle = onAngle;
    triggerTime = millis();
    servo.write(angle);
  }

  void ServoPulse::update()
  {
    if (!servoAttached)
    {
      servo.attach(servoPin);
      servo.write(offAngle);
      servoAttached = true;
    }

    if (angle == onAngle && millis() - triggerTime > holdTime)
    {
      angle = offAngle;
      servo.write(angle);
    }
  }
}
