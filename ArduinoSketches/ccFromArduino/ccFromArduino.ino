#include <MIDI.h>

int lastMapped = 0;

struct MySettings : public midi::DefaultSettings
{
    static const long BaudRate = 115200;
};
MIDI_CREATE_CUSTOM_INSTANCE(HardwareSerial, Serial, MIDI, MySettings);

void setup() {
  // start midi
  MIDI.begin(MIDI_CHANNEL_OMNI);
}

void loop() {
  int value = analogRead(A0);
  int mapped = map(value, 0, 1023, 0, 127);
  if (mapped != lastMapped)
  {
    MIDI.sendControlChange(10, mapped, 1);
    lastMapped = mapped;
  }
}
