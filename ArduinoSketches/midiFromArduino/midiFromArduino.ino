#include <MIDI.h>

struct MySettings : public midi::DefaultSettings
{
    static const long BaudRate = 115200;
};
MIDI_CREATE_CUSTOM_INSTANCE(HardwareSerial, Serial, MIDI, MySettings);

void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);
    MIDI.begin(MIDI_CHANNEL_OMNI);                      // Launch MIDI and listen on all channels
}

void loop()
{
    MIDI.sendNoteOn(42, 127, 1);    // Send a Note (pitch 42, velo 127 on channel 1)
    delay(1000);                // Wait for a second
    MIDI.sendNoteOff(42, 0, 1);
    delay(1000);
}
