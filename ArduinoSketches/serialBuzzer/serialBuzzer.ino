
void setup() {
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  while (Serial.available())
  {
    char c = Serial.read();
    if (c > '0' && c <= '9')
    {
      int cInt = c - '0';
      Serial.print(cInt);
      tone(4, cInt * 100, 500);
    }
  }
}
